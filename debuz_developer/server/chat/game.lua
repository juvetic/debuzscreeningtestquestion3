local Packet = require("chat.packet")

local Game = {}

--------------------------------------------
--- private
--------------------------------------------

local ccu = 0
local users = {}

local function add_user(remote)
  for uid, r in pairs(users) do
    r:send(Packet.make_add_user(remote.uid, remote.user))
  end
end

local function add_previousUser(currentUid)
  for i = currentUid - 1, 1,-1 do
    users[currentUid]:send(Packet.make_add_user(i, users[i].user))
  end
end

local function remove_user(uid)
  for uid, r in pairs(users) do
    r:send(Packet.make_remove_user(uid))
  end
end

local function checkDuplicatedName(currentUid)
  for i = currentUid - 1, 1,-1 do
    if(users[i].user == users[currentUid].user) then
      users[currentUid].user = users[currentUid].user .. "[1]"
    end
  end
end

--------------------------------------------
--- public
--------------------------------------------

function Game.register_user(remote)
  local uid = #users + 1

  remote.uid = uid
  users[uid] = remote

  checkDuplicatedName(uid)
  add_user(remote)
  add_previousUser(uid);

  ccu = ccu + 1
  print("CCU = " .. ccu)
end

function Game.unregister_user(remote)
  local uid = remote.uid

  users[uid] = nil

  remote.uid = nil
  remote.user = nil

  remove_user(uid)

  ccu = ccu - 1
  print("CCU = " .. ccu)
end

function Game.chat(user, message)
  for uid, r in pairs(users) do
    r:send(Packet.make_chat(user, message))
  end
end

function Game.privateChat(remote, receivingUserID, message)
  local receivingUserIDNumber =  tonumber(receivingUserID)
  users[receivingUserIDNumber]:send(Packet.make_privateChat(remote.user, message))

  if(remote.uid ~= receivingUserIDNumber) then
  users[remote.uid]:send(Packet.make_privateChat(remote.user, message))
  end

end

return Game
